LabView source codes for Allan variance and deviation data analysis (LabView 2009, Version 9.0.1).

The code calculates, displays and saves the Allan variance/deviation plots of time-domain data series.
Definition of Allan variance:
"Allan variance is one half of the mean-square of the differences of successive averages of the input array elements of the input array are time-averaged data."

The Allan variance math algorithm is coded according to the algorithm described in:
"Using the Allan variance and power spectral density to characterize DC nanovoltmeters", Th. Witt, IEEE Trans. Instr. Meas. 50, 445 (2001).

The main VI is "Allan_Plots.VI" in the library "Allan-Variance.llb".
The zip-file also contains a test data set "AVAR-test.dat" (ASCII) to show the structure of the expected input data file.


H. Scherer
12.07.2012